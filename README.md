# Money Transfer REST application

Created with JDK 8 and Maven 3, using Grizzly embedded server and Jersey JAX-RS implementation

### How to start

Once the application is fetched from git it can be built with maven

>mvn install

This will fetch dependencies and run basic integration test

To run the app execute
> java -jar target/revolut-1.0-SNAPSHOT-jar-with-dependencies.jar

Base url is http://localhost:8080/revolut/

On start, there're 2 pre-filled accounts and no transactions. Integration test performs a few create operations and may be run separately

## Examples

### Accounts

#### Create an account

The following creates an account and returns the created resource

    POST /account/create
    {
        "balance": {
            value: 100,
            currency: "EUR"
        }
    }

#### List all accounts

The following gets all the accounts

    GET /account/list

#### Get account details

The following gets account details

    GET /account/1
	
### Transactions

#### Create a transaction

The following creates a new transaction if possible (valid accounts and parameters)

    POST /transaction/create
    {
        "sourceId": 1,
        "destinationId": 2,
        "amount": {
            "value": "111",
            "currency": "EUR"
        }
    }

This will return the status, having auto incremented ID.

#### Execute a transaction

Given that a transaction is created, it can be executed as follows:

    PUT /transaction/1/execute

#### List all transactions

The following gets all the transactions

    GET /transaction/list

#### Get transaction details

The following gets transaction details

    GET /transaction/1
