package com.revolut;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Currency;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.junit.BeforeClass;
import org.junit.Test;

import com.revolut.controller.CreateTransactionResult;
import com.revolut.controller.CreateTransactionResult.CreationStatus;
import com.revolut.controller.ExecuteTransactionResult.ExecutionStatus;
import com.revolut.model.Account;
import com.revolut.model.Amount;
import com.revolut.service.Main;
import com.revolut.service.TransactionDTO;

public class IntegrationTest {
	
	@BeforeClass
	public static void startServer() throws IOException {
		Main.startServer();
	}

	@Test
	public void testTransactionList() {
		assertEquals(200, ClientBuilder.newClient().target(Main.BASE_URI + "transaction/list").request().get().getStatus());
	}

	@Test
	public void testTransactionCreate() {
		TransactionDTO t = new TransactionDTO(2, 1, new Amount(new BigDecimal(11), Currency.getInstance("USD")));
		CreateTransactionResult cs = ClientBuilder.newClient().target(Main.BASE_URI + "transaction/create").request(MediaType.APPLICATION_JSON)
				.post(Entity.json(t), CreateTransactionResult.class);
		assertEquals(CreationStatus.SUCCESS, cs.getStatus());
		// System.out.println(cs.getTransactionId());
	}

	@Test
	public void testTransactionExecute() {
		TransactionDTO t = new TransactionDTO(2, 1, new Amount(new BigDecimal(11), Currency.getInstance("USD")));
		CreateTransactionResult cs = ClientBuilder.newClient().target(Main.BASE_URI + "transaction/create").request(MediaType.APPLICATION_JSON)
				.post(Entity.json(t), CreateTransactionResult.class);
		assertEquals(CreationStatus.SUCCESS, cs.getStatus());

		long txId = cs.getTransactionId();
		ExecutionStatus es = ClientBuilder.newClient().target(Main.BASE_URI + "transaction/" + txId + "/execute").request(MediaType.APPLICATION_JSON)
				.put(Entity.json(t) /* whatever */, ExecutionStatus.class);
		assertEquals(ExecutionStatus.SUCCESS, es);
	}
	
	@Test
	public void testAccountCreate() {
		Account a = new Account(0, new Amount(new BigDecimal(5), Currency.getInstance("USD")));
		Account created = ClientBuilder.newClient().target(Main.BASE_URI + "account/create").request(MediaType.APPLICATION_JSON).post(Entity.json(a), Account.class);
		assertTrue(created.getId() > 0);
	}
}
