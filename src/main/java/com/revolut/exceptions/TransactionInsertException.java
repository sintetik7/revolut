package com.revolut.exceptions;

import com.revolut.model.Transaction;

public class TransactionInsertException extends DataAccessException {
	private static final long serialVersionUID = 8252545409516629881L;

	public TransactionInsertException(Transaction transaction) {
		super("Could not insert transaction [" + transaction + "]");
	}
}
