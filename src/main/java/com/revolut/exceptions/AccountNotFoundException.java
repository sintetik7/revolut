package com.revolut.exceptions;

public class AccountNotFoundException extends DataAccessException {
	private static final long serialVersionUID = -758082843306358406L;

	public AccountNotFoundException(Long accountId) {
		super("Could not find account for id [" + accountId + "]");
	}
}
