package com.revolut.exceptions;

public class TransactionNotFoundException extends DataAccessException {
	private static final long serialVersionUID = -4120906891799743972L;

	public TransactionNotFoundException(Long transactionId) {
		super("Could not find transaction for id [" + transactionId + "]");
	}
}
