package com.revolut.exceptions;

public class CouldNotLockResourceException extends DataAccessException {
	private static final long serialVersionUID = 7052411452648536758L;

	public CouldNotLockResourceException(String message) {
		super(message);
	}
}
