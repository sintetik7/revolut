package com.revolut.exceptions;

public class DataAccessException extends Exception {
	private static final long serialVersionUID = 6170916534844383663L;

	public DataAccessException(String message) {
		super(message);
	}
}
