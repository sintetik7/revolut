package com.revolut.model;

import com.revolut.utils.Assert;

public class Account {

	private long id;
	private Amount balance;

	Account() {
		// empty constructor for serializer
	}

	public Account(Amount balance) {
		this(0, balance);
	}

	public Account(long id, Amount balance) {
		Assert.checkNotNull(balance, "balance cannot be null");
		Assert.checkTrue(id >= 0, "id cannot be negative");
		this.id = id;
		this.balance = balance;
	}

	public long getId() {
		return id;
	}

	public Amount getBalance() {
		return balance;
	}

	public Account deposit(Amount amount) {
		return new Account(id, balance.add(amount));
	}

	public Account withdraw(Amount amount) {
		return new Account(id, balance.subtract(amount));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
