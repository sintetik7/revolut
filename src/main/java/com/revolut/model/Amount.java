package com.revolut.model;

import java.math.BigDecimal;
import java.util.Currency;

import com.revolut.utils.Assert;

public class Amount {

	private BigDecimal value;
	private Currency currency;

	Amount() {
		// empty constructor for serializer
	}

	public Amount(BigDecimal value, Currency currency) {
		Assert.checkNotNull(value, "value cannot be null");
		Assert.checkNotNull(currency, "currency cannot be null");
		Assert.checkTrue(value.compareTo(BigDecimal.ZERO) >= 0, "cannot initialize amount to negative value");
		this.value = value;
		this.currency = currency;
	}

	public BigDecimal getValue() {
		return value;
	}

	public Currency getCurrency() {
		return currency;
	}

	public Amount add(Amount amount) {
		Assert.checkTrue(amount.getCurrency().equals(currency), "Cannot add amounts with different currencies");
		return new Amount(amount.value.add(value), currency);
	}

	public Amount subtract(Amount amount) {
		Assert.checkTrue(amount.currency.equals(currency), "Cannot subtract amounts with different currencies");
		Assert.checkTrue(amount.value.compareTo(value) <= 0, "Can subtract only smaller amounts");
		return new Amount(value.subtract(amount.value), currency);
	}

}
