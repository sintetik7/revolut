package com.revolut.model;

import java.util.Currency;

import com.revolut.utils.Assert;

public class Transaction {

	private long id;
	private long sourceId;
	private long destinationId;
	private Amount amount;
	private TransactionStatus status;

	Transaction() {
		// empty constructor for serializer
	}

	public Transaction(long sourceId, long destinationId, Amount amount) {
		this(0, sourceId, destinationId, amount, TransactionStatus.PENDING);
	}

	public Transaction(long id, long sourceId, long destinationId, Amount amount, TransactionStatus status) {
		Assert.checkTrue(sourceId > 0, "sourceId cannot be zero or negative");
		Assert.checkTrue(destinationId > 0, "destinationId cannot be zero or negative");
		Assert.checkTrue(id >= 0, "destinationId cannot be zero or negative");
		Assert.checkNotNull(amount, "amount cannot be null");
		this.id = id;
		this.sourceId = sourceId;
		this.destinationId = destinationId;
		this.amount = amount;
		this.status = status;
	}

	public long getSourceId() {
		return sourceId;
	}

	public long getDestinationId() {
		return destinationId;
	}

	public Amount getAmount() {
		return amount;
	}

	public Currency getCurrency() {
		return amount.getCurrency();
	}

	public TransactionStatus getStatus() {
		return status;
	}

	public long getId() {
		return id;
	}

	public Transaction executed() {
		return new Transaction(id, sourceId, destinationId, amount, TransactionStatus.EXECUTED);
	}

	public Transaction failed() {
		return new Transaction(id, sourceId, destinationId, amount, TransactionStatus.FAILED);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public enum TransactionStatus {
		FAILED, EXECUTED, PENDING
	}
}
