package com.revolut.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import com.revolut.exceptions.CouldNotLockResourceException;
import com.revolut.exceptions.DataAccessException;
import com.revolut.exceptions.TransactionNotFoundException;
import com.revolut.model.Transaction;
import com.revolut.utils.Assert;

public class TransactionRepo {
	public static final long AWAIT_LOCK_MILLISECONDS = 5000L;
	private AtomicLong maxTransactionId = new AtomicLong(0);
	private Map<Long, Transaction> transactionsMap = new ConcurrentHashMap<>();
	private Map<Long, Semaphore> transactionLocks = new ConcurrentHashMap<>();

	public TransactionRepo() {
	}

	public Transaction insert(Transaction transaction) {
		long id = maxTransactionId.incrementAndGet();
		Transaction toInsert = new Transaction(id, transaction.getSourceId(), transaction.getDestinationId(), transaction.getAmount(), Transaction.TransactionStatus.PENDING);
		transactionsMap.put(toInsert.getId(), toInsert);
		transactionLocks.put(toInsert.getId(), new Semaphore(1));
		return toInsert;
	}

	public Transaction update(Transaction transaction) {
		Assert.checkTrue(transaction.getId() > 0, "Cannot update non persisted transaction");
		transactionsMap.put(transaction.getId(), transaction);
		return transaction;
	}

	public Transaction getById(long transactionId) throws DataAccessException {
		Transaction transaction = transactionsMap.get(transactionId);
		if (transaction == null)
			throw new TransactionNotFoundException(transactionId);
		return transaction;
	}

	public void lockById(long transactionId) throws DataAccessException {
		lockById(transactionId, AWAIT_LOCK_MILLISECONDS);
	}

	public void unlockById(long transactionId) throws DataAccessException {
		Semaphore semaphore = transactionLocks.get(transactionId);
		if (semaphore != null && semaphore.availablePermits() == 0)
			semaphore.release();
	}

	public List<Transaction> getAll() throws DataAccessException {
		return transactionsMap.values().stream().collect(Collectors.toList());
	}

	public void deleteAll() throws DataAccessException {
		transactionLocks = new ConcurrentHashMap<>();
		transactionsMap = new ConcurrentHashMap<>();
		maxTransactionId.set(0);
	}

	void lockById(long transactionId, long milliseconds) throws DataAccessException {
		Semaphore semaphore = transactionLocks.get(transactionId);
		if (semaphore == null)
			throw new TransactionNotFoundException(transactionId);
		try {
			if (!semaphore.tryAcquire(milliseconds, TimeUnit.MILLISECONDS))
				throw new CouldNotLockResourceException("Could not lock transaction with id " + transactionId);
		} catch (InterruptedException e) {
			throw new CouldNotLockResourceException("Could not lock transaction with id " + transactionId);
		}
	}

	int getPermitsForLock(long transactionId) {
		return transactionLocks.get(transactionId).availablePermits();
	}

}
