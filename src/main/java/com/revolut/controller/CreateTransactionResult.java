package com.revolut.controller;

import com.revolut.model.Transaction;

public class CreateTransactionResult {

	private long transactionId;
	private CreationStatus status;
	private String detailMessage;

	public CreateTransactionResult() {
		// empty constructor for serializer
	}

	public CreateTransactionResult(Transaction transaction, CreationStatus status) {
		this(transaction, status, "");
	}

	public CreateTransactionResult(Transaction transaction, CreationStatus status, String detailMessage) {
		this.transactionId = transaction.getId();
		this.status = status;
		this.detailMessage = detailMessage;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public CreationStatus getStatus() {
		return status;
	}

	public String getDetailMessage() {
		return detailMessage;
	}

	public enum CreationStatus {
		SUCCESS(2000, "Transaction created successfully"),

		ACCOUNT_NOT_FOUND(4220, "Account id specified was not found."),

		INTERNAL_ERROR(5000, "Unexpected error");

		private final int statusCode;
		private final String description;

		CreationStatus(int statusCode, String description) {
			this.statusCode = statusCode;
			this.description = description;
		}

		public int getStatusCode() {
			return statusCode;
		}

		public String getDescription() {
			return description;
		}
	}
}
