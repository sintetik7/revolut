package com.revolut.controller;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import com.revolut.exceptions.AccountNotFoundException;
import com.revolut.exceptions.CouldNotLockResourceException;
import com.revolut.exceptions.DataAccessException;
import com.revolut.model.Account;
import com.revolut.model.Amount;
import com.revolut.utils.Assert;

public class AccountRepo {
	public static final long AWAIT_LOCK_MILLISECONDS = 5000L;
	private AtomicLong maxAccountId = new AtomicLong(0);
	private Map<Long, Account> accountsMap = new ConcurrentHashMap<>();
	private Map<Long, Semaphore> accountLocks = new ConcurrentHashMap<>();

	public AccountRepo() {
		Account a1 = new Account(0, new Amount(new BigDecimal(5), Currency.getInstance("USD")));
		insert(a1);

		Account a2 = new Account(0, new Amount(new BigDecimal(50), Currency.getInstance("USD")));
		insert(a2);
	}

	public List<Account> getAll() {
		return accountsMap.values().stream().collect(Collectors.toList());
	}

	public Account getById(long accountId) throws DataAccessException {
		Account account = accountsMap.get(accountId);
		if (account == null)
			throw new AccountNotFoundException(accountId);
		return account;
	}

	public Account insert(Account account) {
		long id = maxAccountId.incrementAndGet();
		Account toInsert = new Account(id, account.getBalance());
		accountsMap.put(toInsert.getId(), toInsert);
		accountLocks.put(toInsert.getId(), new Semaphore(1));
		return toInsert;
	}

	public Account update(Account account) throws DataAccessException {
		Assert.checkTrue(account.getId() > 0, "Cannot update non persisted account");
		accountsMap.put(account.getId(), account);
		return account;
	}

	public void lockById(long accountId) throws DataAccessException {
		lockById(accountId, AWAIT_LOCK_MILLISECONDS);
	}

	public void unlockById(long accountId) throws DataAccessException {
		Semaphore semaphore = accountLocks.get(accountId);
		if (semaphore != null && semaphore.availablePermits() == 0)
			semaphore.release();
	}

	public void deleteAll() throws DataAccessException {
		accountsMap = new ConcurrentHashMap<>();
		accountLocks = new ConcurrentHashMap<>();
		maxAccountId.set(0);
	}

	void lockById(long accountId, long timeoutMilliseconds) throws DataAccessException {
		Semaphore semaphore = accountLocks.get(accountId);
		if (semaphore == null)
			throw new AccountNotFoundException(accountId);
		try {
			if (!semaphore.tryAcquire(timeoutMilliseconds, TimeUnit.MILLISECONDS)) {
				throw new CouldNotLockResourceException("Could not lock account with id " + accountId);
			}
		} catch (InterruptedException e) {
			throw new CouldNotLockResourceException("Could not lock account with id " + accountId);
		}
	}

	int getPermitsForLock(long accountId) {
		return accountLocks.get(accountId).availablePermits();
	}
}
