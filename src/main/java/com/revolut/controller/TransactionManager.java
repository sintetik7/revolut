package com.revolut.controller;

import com.revolut.controller.CreateTransactionResult.CreationStatus;
import com.revolut.controller.ExecuteTransactionResult.ExecutionStatus;
import com.revolut.exceptions.AccountNotFoundException;
import com.revolut.exceptions.CouldNotLockResourceException;
import com.revolut.exceptions.DataAccessException;
import com.revolut.exceptions.TransactionNotFoundException;
import com.revolut.model.Account;
import com.revolut.model.Amount;
import com.revolut.model.Transaction;
import com.revolut.model.Transaction.TransactionStatus;
import com.revolut.utils.DoItAllFactory;

public class TransactionManager {
	public static final String INSUFFICIENT_FUNDS_MSG_TEMPLATE = "Account had %s but needed %s to complete the transaction";

	private AccountRepo accountRepo = DoItAllFactory.getAccountRepo();

	private TransactionRepo transactionRepo = DoItAllFactory.getTransactionRepo();

	public CreateTransactionResult createTransaction(Transaction txn) {
		try {
			accountRepo.getById(txn.getSourceId());
			accountRepo.getById(txn.getDestinationId());
			txn = transactionRepo.insert(txn);
			return new CreateTransactionResult(txn, CreationStatus.SUCCESS);
		} catch (AccountNotFoundException anfe) {
			return new CreateTransactionResult(null, CreationStatus.ACCOUNT_NOT_FOUND, anfe.getMessage());
		} catch (DataAccessException dae) {
			return new CreateTransactionResult(null, CreationStatus.INTERNAL_ERROR, dae.getMessage());
		}
	}

	public ExecuteTransactionResult executeTransaction(long transactionId) {
		Transaction txn = null;
		Account srcAccount;
		Account dstAccount;
		try {
			transactionRepo.lockById(transactionId);
			txn = transactionRepo.getById(transactionId);

			if (!txn.getStatus().equals(TransactionStatus.PENDING))
				return new ExecuteTransactionResult(txn, ExecutionStatus.UNCHANGED, "No changes. Transaction was already " + txn.getStatus());

			accountRepo.lockById(txn.getSourceId());
			accountRepo.lockById(txn.getDestinationId());

			srcAccount = accountRepo.getById(txn.getSourceId());
			dstAccount = accountRepo.getById(txn.getDestinationId());

			Amount toWithdraw = txn.getAmount();
			if (!containsSufficientFunds(srcAccount, toWithdraw)) {
				String message = String.format(INSUFFICIENT_FUNDS_MSG_TEMPLATE, srcAccount.getBalance(), toWithdraw);
				return executionFailed(txn, ExecutionStatus.INSUFFICIENT_FUNDS, message);
			}
			Account newSrcAccount = srcAccount.withdraw(toWithdraw);
			Amount toDeposit = toWithdraw;
			Account newDstAccount = dstAccount.deposit(toDeposit);

			accountRepo.update(newSrcAccount);
			accountRepo.update(newDstAccount);
			return executionSucceeded(txn);
		} catch (CouldNotLockResourceException cnlre) {
			return executionFailed(txn, ExecutionStatus.COULD_NOT_ACQUIRE_RESOURCES, cnlre.getMessage());
		} catch (TransactionNotFoundException tnfe) {
			return executionFailed(txn, ExecutionStatus.TRANSACTION_NOT_FOUND, tnfe.getMessage());
		} catch (AccountNotFoundException anfe) {
			return executionFailed(txn, ExecutionStatus.ACCOUNT_NOT_FOUND, anfe.getMessage());
		} catch (DataAccessException e) {
			return executionFailed(txn, ExecutionStatus.INTERNAL_ERROR, e.getMessage());
		} finally {
			unlockResources(transactionId, txn);
		}
	}

	private void unlockResources(long transactionId, Transaction txn) {
		try {
			transactionRepo.unlockById(transactionId);
			if (txn != null) {
				accountRepo.unlockById(txn.getSourceId());
				accountRepo.unlockById(txn.getDestinationId());
			}
		} catch (DataAccessException e) {
			throw new RuntimeException("Error unlocking the resources. This should never occur", e);
		}
	}

	private boolean containsSufficientFunds(Account account, Amount neededBalance) {
		return account.getBalance().getValue().compareTo(neededBalance.getValue()) >= 0;
	}

	private ExecuteTransactionResult executionFailed(Transaction txn, ExecutionStatus status, String message) {
		if (txn != null) {
			txn = txn.failed();
			transactionRepo.update(txn);
		}
		return new ExecuteTransactionResult(txn, status, message);
	}

	private ExecuteTransactionResult executionSucceeded(Transaction txn) {
		txn = txn.executed();
		transactionRepo.update(txn);
		return new ExecuteTransactionResult(txn, ExecutionStatus.SUCCESS);
	}
}
