package com.revolut.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.revolut.controller.AccountRepo;
import com.revolut.exceptions.DataAccessException;
import com.revolut.model.Account;
import com.revolut.utils.DoItAllFactory;

@Path("account")
public class AccountService {
	private AccountRepo accountRepo = DoItAllFactory.getAccountRepo();

	@GET
	@Path("list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Account> getAllAccounts() {
		return accountRepo.getAll();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Account getAccount(@PathParam("id") long id) throws DataAccessException {
		return accountRepo.getById(id);
	}
	
	@POST
	@Path("create")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Account create(Account account) {
		return accountRepo.insert(account);
	}
}
