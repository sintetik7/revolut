package com.revolut.service;

import java.io.IOException;
import java.net.URI;
import java.util.Optional;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

public class Main {
	public static final String DEFAULT_HOST = "localhost";
	public static final String DEFAULT_PORT = "8080";
	public static final String DEFAULT_APP = "revolut";

	public static final String BASE_URI;
	public static final Optional<String> host;
	public static final Optional<String> port;

	static {
		host = Optional.ofNullable(System.getenv("HOSTNAME"));
		port = Optional.ofNullable(System.getenv("PORT"));
		BASE_URI = "http://" + host.orElse("localhost") + ":" + port.orElse(DEFAULT_PORT) + "/" + DEFAULT_APP + "/";
	}

	/**
	 * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
	 * @return Grizzly HTTP server.
	 */
	public static HttpServer startServer() {
		final ResourceConfig rc = new ResourceConfig().packages("com.revolut.service");
		rc.register(JacksonFeature.class);
		return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
	}

	public static void main(String[] args) throws IOException {
		final HttpServer server = startServer();
		System.out.println(String.format("Jersey app started with WADL available at %sapplication.wadl\nHit enter to stop it...", BASE_URI));
		System.in.read();
		server.shutdownNow();
	}
}
