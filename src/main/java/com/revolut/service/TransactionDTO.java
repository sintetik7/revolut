package com.revolut.service;

import com.revolut.model.Amount;

public class TransactionDTO {

	private long sourceId;
	private long destinationId;
	private Amount amount;

	TransactionDTO() {
		// empty constructor for serializer
	}

	public TransactionDTO(long sourceId, long destinationId, Amount amount) {
		this.sourceId = sourceId;
		this.destinationId = destinationId;
		this.amount = amount;
	}

	public long getSourceId() {
		return sourceId;
	}

	public long getDestinationId() {
		return destinationId;
	}

	public Amount getAmount() {
		return amount;
	}
}
