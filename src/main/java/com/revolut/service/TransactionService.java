package com.revolut.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.revolut.controller.CreateTransactionResult;
import com.revolut.controller.ExecuteTransactionResult.ExecutionStatus;
import com.revolut.controller.TransactionManager;
import com.revolut.controller.TransactionRepo;
import com.revolut.exceptions.DataAccessException;
import com.revolut.model.Transaction;
import com.revolut.utils.DoItAllFactory;

@Path("transaction")
public class TransactionService {
	private TransactionRepo transactionRepo = DoItAllFactory.getTransactionRepo();

	private TransactionManager transactionManager = DoItAllFactory.getTransactionManager();

	@GET
	@Path("list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Transaction> getAll() throws DataAccessException {
		return transactionRepo.getAll();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Transaction getTransaction(@PathParam("id") long id) throws DataAccessException {
		return transactionRepo.getById(id);
	}

	@POST
	@Path("create")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CreateTransactionResult create(TransactionDTO tdto) {
		return transactionManager.createTransaction(new Transaction(tdto.getSourceId(), tdto.getDestinationId(), tdto.getAmount()));
	}

	@PUT
	@Path("{id}/execute")
	@Produces(MediaType.APPLICATION_JSON)
	public ExecutionStatus execute(@PathParam("id") long id) {
		return transactionManager.executeTransaction(id).getStatus();
	}

}
