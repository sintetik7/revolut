package com.revolut.utils;

import com.revolut.controller.AccountRepo;
import com.revolut.controller.TransactionManager;
import com.revolut.controller.TransactionRepo;

// No spring, no container...
public class DoItAllFactory {

	public static AccountRepo getAccountRepo() {
		return accountRepo;
	}

	public static TransactionRepo getTransactionRepo() {
		return transactionRepo;
	}

	public static TransactionManager getTransactionManager() {
		return transactionManager;
	}

	private static final AccountRepo accountRepo = new AccountRepo();
	private static final TransactionRepo transactionRepo = new TransactionRepo();
	private static final TransactionManager transactionManager = new TransactionManager();

	private DoItAllFactory() {
	}
}
